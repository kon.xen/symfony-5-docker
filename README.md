# Symfony 5 docker containers

A Proof-of-concept of a running Symfony 5 application inside containers


clone the docker  in to a folder docker
```
git clone git@gitlab.com:kon.xen/symfony-5-docker.git docker
```

then clone your project in to a folder src

```
git clone --your project-- src
```
cd in to the docker folder and ...
```
cd docker
docker-compose build
docker compose up
```

## Compose

### Database (MariaDB)

...

### PHP (PHP-FPM)

Composer is included. Run composer form the docker folder

```
docker-compose run php-fpm composer 
```

To run fixtures

```
docker-compose run php-fpm bin/console doctrine:fixtures:load
```

### Webserver (Nginx)

...
